package log;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestLogLevel {

    @Test
    public void toString_returnEnumName() {
        assertEquals("ERROR", LogLevel.ERROR.toString());
        assertEquals("INFO", LogLevel.INFO.toString());
        assertEquals("DEBUG", LogLevel.DEBUG.toString());
        assertEquals("TRACE", LogLevel.TRACE.toString());
    }

    @Test
    public void isMorePermissiveThan_none_returnFalse() {
        assertFalse(LogLevel.NONE.isMorePermissiveThan(LogLevel.ERROR));
        assertFalse(LogLevel.NONE.isMorePermissiveThan(LogLevel.INFO));
        assertFalse(LogLevel.NONE.isMorePermissiveThan(LogLevel.DEBUG));
        assertFalse(LogLevel.NONE.isMorePermissiveThan(LogLevel.TRACE));
        assertFalse(LogLevel.NONE.isMorePermissiveThan(LogLevel.ALL));
    }

    @Test
    public void isMorePermissiveThan_lowerLevel_returnTrue() {
        assertTrue(LogLevel.INFO.isMorePermissiveThan(LogLevel.ERROR));
        assertTrue(LogLevel.DEBUG.isMorePermissiveThan(LogLevel.INFO));
        assertTrue(LogLevel.TRACE.isMorePermissiveThan(LogLevel.DEBUG));
    }

    @Test
    public void isMorePermissiveThan_sameLevel_returnTrue() {
        assertTrue(LogLevel.ERROR.isMorePermissiveThan(LogLevel.ERROR));
        assertTrue(LogLevel.INFO.isMorePermissiveThan(LogLevel.INFO));
        assertTrue(LogLevel.DEBUG.isMorePermissiveThan(LogLevel.DEBUG));
        assertTrue(LogLevel.TRACE.isMorePermissiveThan(LogLevel.TRACE));
    }

    @Test
    public void isMorePermissiveThan_higherLevel_returnFalse() {
        assertFalse(LogLevel.ERROR.isMorePermissiveThan(LogLevel.INFO));
        assertFalse(LogLevel.INFO.isMorePermissiveThan(LogLevel.DEBUG));
        assertFalse(LogLevel.DEBUG.isMorePermissiveThan(LogLevel.TRACE));
    }

    @Test
    public void isMorePermissiveThan_all_returnTrue() {
        assertTrue(LogLevel.ALL.isMorePermissiveThan(LogLevel.NONE));
        assertTrue(LogLevel.ALL.isMorePermissiveThan(LogLevel.ERROR));
        assertTrue(LogLevel.ALL.isMorePermissiveThan(LogLevel.INFO));
        assertTrue(LogLevel.ALL.isMorePermissiveThan(LogLevel.DEBUG));
        assertTrue(LogLevel.ALL.isMorePermissiveThan(LogLevel.TRACE));
    }

}
