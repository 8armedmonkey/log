package log;

import java.io.Serializable;

public class LogEvent implements Serializable {

    private final long timeMillis;
    private final LogLevel logLevel;
    private final String tag;
    private final String message;
    private final Throwable throwable;

    public LogEvent(
            long timeMillis,
            LogLevel logLevel,
            String tag,
            String message) {

        this(timeMillis, logLevel, tag, message, null);
    }

    public LogEvent(
            long timeMillis,
            LogLevel logLevel,
            String tag,
            Throwable throwable) {

        this(timeMillis, logLevel, tag, null, throwable);
    }

    public LogEvent(
            long timeMillis,
            LogLevel logLevel,
            String tag,
            String message,
            Throwable throwable) {

        this.timeMillis = timeMillis;
        this.logLevel = logLevel;
        this.tag = tag;
        this.message = message;
        this.throwable = throwable;
    }

    public long getTimeMillis() {
        return timeMillis;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public String getTag() {
        return tag;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public static class Builder {

        private long timeMillis;
        private LogLevel logLevel;
        private String tag;
        private String message;
        private Throwable throwable;

        public Builder timeMillis(long timeMillis) {
            this.timeMillis = timeMillis;
            return this;
        }

        public Builder logLevel(LogLevel logLevel) {
            this.logLevel = logLevel;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder throwable(Throwable throwable) {
            this.throwable = throwable;
            return this;
        }

        public LogEvent build() {
            return new LogEvent(timeMillis, logLevel, tag, message, throwable);
        }

    }

}
