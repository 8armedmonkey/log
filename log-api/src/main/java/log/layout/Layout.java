package log.layout;

import log.LogEvent;

public interface Layout {

    /**
     * Layout the log event before being outputted.
     */
    String process(LogEvent event);

}
