package log;

import log.appender.Appender;
import org.jetbrains.annotations.Nullable;

public interface Logger {

    /**
     * Set the log level.
     */
    void setLogLevel(LogLevel logLevel);

    /**
     * Log error message (minimum log level: ERROR).
     */
    void e(String tag, String format, @Nullable Object... args);

    /**
     * Log error message (minimum log level: ERROR). It will print the stack trace.
     */
    void e(String tag, Throwable throwable);

    /**
     * Log generic message (minimum log level: INFO).
     */
    void i(String tag, String format, @Nullable Object... args);

    /**
     * Log debug message (minimum log level: DEBUG).
     */
    void d(String tag, String format, @Nullable Object... args);

    /**
     * Log the moment of entering an operation / a method (log level: TRACE).
     */
    void enter(String tag, String format, @Nullable Object... args);

    /**
     * Log the moment of exiting an operation / a method (log level: TRACE).
     */
    void exit(String tag, String format, @Nullable Object... args);

    /**
     * Start the logger.
     */
    void start();

    /**
     * Shutdown the logger.
     */
    void shutdown();

    /**
     * Add an appender to receive log event.
     */
    void addAppender(Appender appender);

    /**
     * Remove a previously added appender.
     */
    void removeAppender(Appender appender);

    /**
     * Remove a previously added appender (matched by the name).
     *
     * @param name The name of the appender to be removed.
     */
    void removeAppender(String name);

    /**
     * Check if the logger contains a certain appender.
     */
    boolean containAppender(Appender appender);

    /**
     * Check if the logger contains a certain appender (matched by the name).
     */
    boolean containAppender(String name);

}
