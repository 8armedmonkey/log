package log.appender;

/**
 * Appender that appends log events to the console.
 */
public interface ConsoleAppender extends Appender {
}
