package log.appender;

import log.LogEvent;
import log.layout.Layout;

public interface Appender {

    /**
     * Append the log event to the target.
     */
    void append(LogEvent event);

    /**
     * Get the process of the appender.
     */
    Layout getLayout();

    /**
     * Set the process for the appender.
     */
    void setLayout(Layout layout);

    /**
     * Get the name of the appender.
     */
    String getName();

    /**
     * Set the name of the appender.
     */
    void setName(String name);

}
