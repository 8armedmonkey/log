package log.appender;

/**
 * Appender that appends log events to the specified file.
 */
public interface FileAppender extends Appender {

    /**
     * Get the current file name used by this appender.
     */
    String getFileName();

    /**
     * Set the new file name for this appender.
     * The current buffer will be flushed and written first to the old file.
     */
    void setFileName(String fileName);

    /**
     * Flush the buffer used by this appender.
     */
    void flush();

    /**
     * Flush and close this appender.
     */
    void close();

}
