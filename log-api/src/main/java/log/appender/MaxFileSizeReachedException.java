package log.appender;

public class MaxFileSizeReachedException extends Exception {

    public MaxFileSizeReachedException(String message) {
        super(message);
    }

}
