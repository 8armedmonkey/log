package log.appender;

public class AppendException extends RuntimeException {

    public AppendException(Throwable t) {
        super(t);
    }

}
