package log.formatter;

import org.jetbrains.annotations.Nullable;

public interface MessageFormatter {

    /**
     * Process the format and arguments into a i message.
     *
     * @param format Format, placeholder is expressed as '{}'.
     * @param args   Arguments, most data types will be converted to {@link String},
     *               byte array will be converted to hexadecimal string.
     * @return Formatted message.
     */
    String format(String format, @Nullable Object... args);

}
