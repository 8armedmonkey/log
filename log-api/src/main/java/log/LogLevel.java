package log;

/**
 * A higher log level will allow messages with lower log level(s) to be logged.
 * Order NONE < ERROR < INFO < DEBUG < ALL.
 */
public enum LogLevel {

    NONE(0), ERROR(1), INFO(2), DEBUG(3), TRACE(4), ALL(5);

    int level;

    LogLevel(int level) {
        this.level = level;
    }

    public boolean isMorePermissiveThan(LogLevel logLevel) {
        return level >= logLevel.level;
    }

    @Override
    public String toString() {
        return name();
    }

}
