package log.benchmark.thread;

import log.LogEvent;
import log.LogLevel;
import log.appender.FileAppender;
import log.appender.FileAppenderImpl;
import log.layout.LineLayout;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.out;
import static log.LogLevel.DEBUG;
import static log.util.FileUtils.deleteFile;
import static log.util.FileUtils.recreateFile;

public class MultiThreadTest {

    private static final int NUMBER_OF_THREADS = 5;
    private static final int NUMBER_OF_WRITE_ATTEMPTS = 1000;

    public static void main(String[] args) throws IOException, InterruptedException {
        String fileName = "src/main/resources/appender-multi-thread.txt";
        FileAppender appender = new FileAppenderImpl("FILE", new LineLayout(), fileName, 1024 * 1024 * 1024);
        Thread[] threads = new Thread[NUMBER_OF_THREADS];
        Map<String, Integer> threadWriteCounts = new HashMap<>();

        recreateFile(fileName);

        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            String threadName = String.format("Thread %d", (i + 1));
            threads[i] = createNewThread(threadName, appender, NUMBER_OF_WRITE_ATTEMPTS);
            threadWriteCounts.put(threadName, 0);
        }

        startThreads(threads);
        joinThreads(threads);

        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;

        while ((line = reader.readLine()) != null) {
            Integer count = threadWriteCounts.get(line);
            threadWriteCounts.put(line, count + 1);
        }

        for (Thread thread : threads) {
            String threadName = thread.getName();
            out.println(String.format("Write count for %s: %d", threadName, threadWriteCounts.get(threadName)));
        }

        deleteFile(fileName);
    }

    private static Thread createNewThread(String threadName, FileAppender appender, int writeAttempts) {
        return new Thread(() -> {

            for (int i = 0; i < writeAttempts; i++) {
                LogEvent e = new LogEvent.Builder()
                        .timeMillis(currentTimeMillis())
                        .logLevel(DEBUG)
                        .tag("Test")
                        .message(Thread.currentThread().getName())
                        .build();
                appender.append(e);
            }

            appender.flush();

        }, threadName);
    }

    private static void startThreads(Thread[] threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private static void joinThreads(Thread[] threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

}
