package log.benchmark.logger;

import log.LogLevel;
import log.Logger;
import log.LoggerImpl;
import log.appender.ConsoleAppenderImpl;
import log.appender.FileAppenderImpl;
import log.formatter.MessageFormatterImpl;
import log.layout.ColorLineLayout;
import log.layout.LineLayout;

import java.util.Random;

public class LoggerTest {

    public static void main(String[] args) throws InterruptedException {
        Logger logger = new LoggerImpl(LogLevel.ALL, new MessageFormatterImpl());
        logger.addAppender(new ConsoleAppenderImpl("Console", new ColorLineLayout()));
        logger.addAppender(new FileAppenderImpl("File", new LineLayout(), "src/main/resources/log-test.txt", 1024 * 1024 * 1024));

        Thread[] threads = new Thread[10];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new TestLogging(String.format("Thread-%d", i), logger, 10));
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        logger.shutdown();
    }

    static class TestLogging implements Runnable {

        final String name;
        final Logger logger;
        final int totalAttempts;
        final Random random;
        int numOfAttempts;

        TestLogging(String name, Logger logger, int totalAttempts) {
            this.name = name;
            this.logger = logger;
            this.totalAttempts = totalAttempts;
            this.random = new Random();
            this.numOfAttempts = 0;
        }

        @Override
        public void run() {
            while (numOfAttempts < totalAttempts) {
                int d = random.nextInt(400) + 100;
                int n = random.nextInt(1000) + 500;

                for (int i = 0; i < n; i++) {
                    int randomLogLevel = (int) (Math.random() * 4 + 1);

                    switch (randomLogLevel) {
                        case 1:
                            logger.e("Test", "Thread '{}': This is a test error message ({} out of {}).", name, i, n);
                            break;
                        case 2:
                            int byteLength = (random.nextInt(32) + 16) * 16;
                            byte[] bytes = new byte[byteLength];

                            random.nextBytes(bytes);
                            logger.i("Test", "Thread '{}': This is a test info message, bytes: {} ({} out of {}).", name, bytes, i, n);
                            break;
                        case 3:
                            logger.d("Test", "Thread '{}': This is a test debug message ({} out of {}).", name, i, n);
                            break;
                        case 4:
                            logger.enter("Test", "Thread '{}': This is a test enter message ({} out of {}).", name, i, n);
                            logger.exit("Test", "Thread '{}': This is a test exit message ({} out of {}).", name, i, n);
                            break;
                    }
                }

                try {
                    logger.d("Test", "Thread '{}' is going to sleep for {} ms.", name, d);
                    Thread.sleep(d);
                    logger.d("Test", "Thread '{}' is awaken.", name);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                numOfAttempts++;
            }
        }

    }

}
