package log.benchmark.appender;

import log.LogEvent;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import log.layout.Layout;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class FileAppenderImpl2 extends BaseAppender implements FileAppender {

    private static final int DEFAULT_BUFFER_SIZE = 8 * 1024;

    private String fileName;
    private int bufferSize;
    private PrintStream printStream;

    public FileAppenderImpl2(Layout layout, String fileName) {
        this(layout, fileName, DEFAULT_BUFFER_SIZE);
    }

    public FileAppenderImpl2(Layout layout, String fileName, int bufferSize) {
        this.layout = layout;
        this.fileName = fileName;
        this.bufferSize = bufferSize;
    }

    @Override
    public synchronized void append(LogEvent event) {
        try {
            getPrintStream().print(layout.process(event));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return FileAppenderImpl2.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        if (!this.fileName.equals(fileName)) {
            close();
            this.fileName = fileName;
        }
    }

    @Override
    public synchronized void flush() {
        if (printStream != null) {
            printStream.flush();
        }
    }

    @Override
    public synchronized void close() {
        flush();

        if (printStream != null) {
            printStream.close();
            printStream = null;
        }
    }

    private synchronized PrintStream getPrintStream() throws IOException {
        if (printStream == null) {
            FileOutputStream fos = new FileOutputStream(fileName, true);
            printStream = new PrintStream(new BufferedOutputStream(fos, bufferSize));
        }
        return printStream;
    }

}
