package log.benchmark.appender;

import log.LogEvent;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import log.layout.Layout;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileAppenderImpl4 extends BaseAppender implements FileAppender {

    private static final int DEFAULT_BUFFER_SIZE = 8 * 1024;

    private String fileName;
    private int bufferSize;
    private BufferedWriter writer;

    public FileAppenderImpl4(Layout layout, String fileName) {
        this(layout, fileName, DEFAULT_BUFFER_SIZE);
    }

    public FileAppenderImpl4(Layout layout, String fileName, int bufferSize) {
        this.layout = layout;
        this.fileName = fileName;
        this.bufferSize = bufferSize;
    }

    @Override
    public synchronized void append(LogEvent event) {
        try {
            final BufferedWriter writer = getBufferedWriter();
            writer.write(layout.process(event));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return FileAppenderImpl4.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        if (!this.fileName.equals(fileName)) {
            close();
            this.fileName = fileName;
        }
    }

    @Override
    public synchronized void flush() {
        if (writer != null) {
            try {
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void close() {
        flush();

        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            writer = null;
        }
    }

    private synchronized BufferedWriter getBufferedWriter() throws IOException {
        if (writer == null) {
            writer = new BufferedWriter(new FileWriter(fileName, true), bufferSize);
        }
        return writer;
    }

}
