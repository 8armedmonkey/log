package log.benchmark.appender;

import log.LogEvent;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import log.layout.Layout;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class FileAppenderImpl1 extends BaseAppender implements FileAppender {

    private String fileName;
    private PrintWriter writer;

    public FileAppenderImpl1(Layout layout, String fileName) {
        this.layout = layout;
        this.fileName = fileName;
    }

    @Override
    public synchronized void append(LogEvent event) {
        try {
            getPrintWriter().print(layout.process(event));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return FileAppenderImpl1.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        if (!this.fileName.equals(fileName)) {
            close();
            this.fileName = fileName;
        }
    }

    @Override
    public synchronized void flush() {
        if (writer != null) {
            writer.flush();
        }
    }

    @Override
    public synchronized void close() {
        flush();

        if (writer != null) {
            writer.close();
            writer = null;
        }
    }

    private synchronized PrintWriter getPrintWriter() throws IOException {
        if (writer == null) {
            writer = new PrintWriter(new FileOutputStream(fileName, true));
        }
        return writer;
    }

}
