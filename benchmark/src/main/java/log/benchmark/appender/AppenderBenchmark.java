package log.benchmark.appender;

import bb.util.MemoryMeasurer;
import log.LogEvent;
import log.appender.FileAppender;
import log.layout.LineLayout;

import java.io.IOException;

import static java.lang.String.format;
import static java.lang.System.out;
import static log.LogLevel.DEBUG;
import static log.util.FileUtils.deleteFile;
import static log.util.FileUtils.recreateFile;

public class AppenderBenchmark {

    private static final int ATTEMPTS = 100000;

    private static final FileAppender[] APPENDERS = {
            new FileAppenderImpl1(new LineLayout(), "src/main/resources/appender-impl1.txt"),
            new FileAppenderImpl2(new LineLayout(), "src/main/resources/appender-impl2.txt"),
            new FileAppenderImpl3(new LineLayout(), "src/main/resources/appender-impl3.txt"),
            new FileAppenderImpl4(new LineLayout(), "src/main/resources/appender-impl4.txt"),
            new FileAppenderImpl5(new LineLayout(), "src/main/resources/appender-impl5.txt", 32 * 1024 * 1024),
            new FileAppenderImplLog4j("src/main/resources/appender-impl6.txt")
    };

    public static void main(String[] args) throws IOException {
        simpleBenchmark();
    }

    private static void simpleWarmUpJvm() throws IOException {
        String text = "Text text text text text text text text text text text text text text text text.";
        LogEvent event = new LogEvent.Builder()
                .timeMillis(System.currentTimeMillis())
                .logLevel(DEBUG)
                .tag("Test")
                .message(text)
                .build();

        long n = 1;

        for (long start = System.nanoTime(); System.nanoTime() - start < 10.0 * 1e9; n *= 2) {
            for (int i = 0; i < n; i++) {
                for (FileAppender appender : APPENDERS) {
                    recreateFile(appender.getFileName());

                    for (int j = 0; j < ATTEMPTS; j++) {
                        appender.append(event);
                    }

                    appender.flush();
                    appender.close();

                    deleteFile(appender.getFileName());
                }
            }
        }
    }

    private static void simpleBenchmark() throws IOException {
        MemoryMeasurer.restoreJvm();

        out.println("Simple warm up JVM");
        out.println();
        simpleWarmUpJvm();

        String text = "Text text text text text text text text text text text text text text text text.";
        LogEvent event = new LogEvent.Builder()
                .timeMillis(System.currentTimeMillis())
                .logLevel(DEBUG)
                .tag("Test")
                .message(text)
                .build();

        for (int i = 0, size = APPENDERS.length; i < size; i++) {
            out.println(format("Simple benchmarking appender #%d:", i + 1));

            recreateFile(APPENDERS[i].getFileName());

            long start = System.currentTimeMillis();

            for (int j = 0; j < ATTEMPTS; j++) {
                APPENDERS[i].append(event);
            }

            APPENDERS[i].flush();
            APPENDERS[i].close();

            long end = System.currentTimeMillis();
            long duration = end - start;

            deleteFile(APPENDERS[i].getFileName());

            out.println(String.format("Duration #%d: %d ms.", i + 1, duration));
            out.println("-----");
        }
    }

}
