package log.benchmark.appender;

import log.LogEvent;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import log.layout.Layout;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class FileAppenderImpl3 extends BaseAppender implements FileAppender {

    private String fileName;
    private PrintStream printStream;

    public FileAppenderImpl3(Layout layout, String fileName) {
        this.layout = layout;
        this.fileName = fileName;
    }

    @Override
    public synchronized void append(LogEvent event) {
        try {
            getPrintStream().print(layout.process(event));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return FileAppenderImpl3.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        if (!this.fileName.equals(fileName)) {
            close();
            this.fileName = fileName;
        }
    }

    @Override
    public synchronized void flush() {
        if (printStream != null) {
            printStream.flush();
        }
    }

    @Override
    public synchronized void close() {
        flush();

        if (printStream != null) {
            printStream.close();
            printStream = null;
        }
    }

    private synchronized PrintStream getPrintStream() throws IOException {
        if (printStream == null) {
            FileOutputStream fos = new FileOutputStream(fileName, true);
            printStream = new PrintStream(fos);
        }
        return printStream;
    }

}
