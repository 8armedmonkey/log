package log.benchmark.appender;

import log.LogEvent;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.SimpleMessage;

public class FileAppenderImplLog4j extends BaseAppender implements FileAppender {

    private static final int DEFAULT_BUFFER_SIZE = 8 * 1024;

    private String fileName;
    private int bufferSize;
    private org.apache.logging.log4j.core.appender.FileAppender appender;

    public FileAppenderImplLog4j(String fileName) {
        this(fileName, DEFAULT_BUFFER_SIZE);
    }

    public FileAppenderImplLog4j(String fileName, int bufferSize) {
        this.fileName = fileName;
        this.bufferSize = bufferSize;
    }

    @Override
    public synchronized void append(LogEvent event) {
        Log4jLogEvent logEvent = Log4jLogEvent.newBuilder()
                .setMessage(new SimpleMessage(event.getMessage()))
                .build();

        getAppender().append(logEvent);
    }

    @Override
    public String getName() {
        return FileAppenderImplLog4j.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        if (!this.fileName.equals(fileName)) {
            close();
            this.fileName = fileName;
        }
    }

    @Override
    public synchronized void flush() {
        if (appender != null) {
            appender.getManager().flush();
        }
    }

    @Override
    public synchronized void close() {
        if (appender != null) {
            appender.stop();
            appender = null;
        }
    }

    private synchronized org.apache.logging.log4j.core.appender.FileAppender getAppender() {
        if (appender == null) {
            appender = org.apache.logging.log4j.core.appender.FileAppender.newBuilder()
                    .withFileName(fileName)
                    .withAppend(true)
                    .withBufferedIo(true)
                    .withBufferSize(bufferSize)
                    .build();

            appender.start();
        }
        return appender;
    }

}
