package log.benchmark.appender;

import log.LogEvent;
import log.LogLevel;
import log.appender.Appender;
import log.layout.LineLayout;
import log.util.FileUtils;

import static java.lang.System.currentTimeMillis;

public class TestMaxFileSize {

    public static void main(String[] args) {
        try {
            String path = "src/main/resources/max-file-size-test.txt";
            FileUtils.recreateFile(path);

            Appender a = new FileAppenderImpl5(new LineLayout(), path, 16 * 1024);

            for (int i = 0; i < 100000; i++) {
                a.append(new LogEvent.Builder()
                        .logLevel(LogLevel.DEBUG)
                        .tag("Tag")
                        .message("Message - This is a test log message.")
                        .timeMillis(currentTimeMillis())
                        .build());
            }
        } catch (Throwable t) {
            System.out.println("Error appending logs: " + t.getMessage());
        }
    }

}
