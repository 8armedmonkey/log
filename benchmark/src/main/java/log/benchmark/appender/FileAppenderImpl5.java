package log.benchmark.appender;

import log.LogEvent;
import log.appender.AppendException;
import log.appender.BaseAppender;
import log.appender.FileAppender;
import log.appender.MaxFileSizeReachedException;
import log.layout.Layout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class FileAppenderImpl5 extends BaseAppender implements FileAppender {

    private static final int DEFAULT_BUFFER_SIZE = 8 * 1024;
    private static final long UNKNOWN_FILE_SIZE = -1;

    private String fileName;
    private ByteBuffer buffer;
    private OutputStream outputStream;
    private File file;
    private long fileSize;
    private long maxFileSize;

    public FileAppenderImpl5(Layout layout, String fileName, long maxFileSize) {
        this(layout, fileName, maxFileSize, DEFAULT_BUFFER_SIZE);
    }

    public FileAppenderImpl5(Layout layout, String fileName, long maxFileSize, int bufferSize) {
        this.layout = layout;
        this.maxFileSize = maxFileSize;
        this.buffer = ByteBuffer.allocate(bufferSize);
        this.setFileNameInternal(fileName);
    }

    @Override
    public synchronized void append(LogEvent event) {
        checkCurrentFileSize();

        byte[] bytes = layout.process(event).getBytes();

        if (bytes.length > buffer.capacity()) {
            flush();

            if (fileSize + bytes.length > maxFileSize) {
                throwMaxFileSizeReachedException();
            } else {
                writeToDestination(bytes, 0, bytes.length);
            }
        } else {
            if (fileSize + (buffer.limit() - buffer.remaining() + bytes.length) > maxFileSize) {
                flush();
                throwMaxFileSizeReachedException();
            }

            if (bytes.length > buffer.remaining()) {
                flush();
            }
            buffer.put(bytes);
        }
    }

    @Override
    public String getName() {
        return FileAppenderImpl5.class.getName();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public synchronized void setFileName(String fileName) {
        File newFile = new File(fileName);

        if (!this.file.getAbsolutePath().equals(newFile.getAbsolutePath())) {
            close();
            setFileNameInternal(fileName);
        }
    }

    @Override
    public synchronized void flush() {
        flushBuffer();
        flushOutputStream();
    }

    @Override
    public synchronized void close() {
        flush();

        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        reset();
    }

    private synchronized void setFileNameInternal(String fileName) {
        this.fileName = fileName;
        this.file = new File(fileName);
        this.fileSize = UNKNOWN_FILE_SIZE;
    }

    private void checkCurrentFileSize() throws AppendException {
        if (fileSize == UNKNOWN_FILE_SIZE) {
            fileSize = file.length();
        }

        if (fileSize >= maxFileSize) {
            throwMaxFileSizeReachedException();
        }
    }

    private void reset() {
        outputStream = null;
        fileSize = 0;
    }

    private synchronized void flushBuffer() {
        buffer.flip();

        if (buffer.limit() > 0) {
            writeToDestination(buffer.array(), 0, buffer.limit());
        }
        buffer.clear();
    }

    private synchronized void flushOutputStream() {
        if (outputStream != null) {
            try {
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("SameParameterValue")
    private synchronized void writeToDestination(byte[] bytes, int index, int limit) {
        try {
            getOutputStream().write(bytes, index, limit);
            fileSize += bytes.length;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private OutputStream getOutputStream() throws IOException {
        if (outputStream == null) {
            outputStream = new FileOutputStream(file, true);
        }
        return outputStream;
    }

    private void throwMaxFileSizeReachedException() throws AppendException {
        String message = String.format("Max. file size reached, file size: %d, max file size: %d",
                fileSize, maxFileSize);

        throw new AppendException(new MaxFileSizeReachedException(message));
    }

}
