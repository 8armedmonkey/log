package log.benchmark.formatter;

import log.formatter.MessageFormatter;
import org.apache.logging.log4j.message.ParameterizedMessage;

public class MessageFormatterImplLog4J implements MessageFormatter {

    @Override
    public String format(String format, Object... args) {
        return ParameterizedMessage.format(format, args);
    }

}
