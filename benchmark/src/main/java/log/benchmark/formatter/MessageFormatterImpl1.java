package log.benchmark.formatter;

import log.formatter.MessageFormatter;
import log.util.Hex;

public class MessageFormatterImpl1 implements MessageFormatter {

    private static final String NULL_STR = "null";

    @Override
    public String format(String format, Object... args) {
        StringBuilder builder = new StringBuilder(format);
        int argIndex = 0;
        int index;

        while ((index = builder.indexOf("{}")) != -1) {
            if (argIndex < args.length) {
                if (args[argIndex] instanceof byte[]) {
                    builder.replace(index, index + 2, Hex.toHexString((byte[]) args[argIndex]));
                } else {
                    builder.replace(index, index + 2, String.valueOf(args[argIndex]));
                }

                argIndex++;

            } else {
                builder.replace(index, index + 2, NULL_STR);
            }
        }
        return builder.toString();
    }

}
