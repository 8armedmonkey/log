package log.benchmark.formatter;

import log.formatter.MessageFormatter;
import log.util.Hex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageFormatterImpl3 implements MessageFormatter {

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("(\\{})");
    private static final String NULL_STR = "null";

    @Override
    public String format(String format, Object... args) {
        Matcher m = PLACEHOLDER_PATTERN.matcher(format);
        StringBuffer buffer = new StringBuffer();
        int argIndex = 0;

        while (m.find()) {
            if (argIndex < args.length) {
                if (args[argIndex] instanceof byte[]) {
                    m.appendReplacement(buffer, Hex.toHexString((byte[]) args[argIndex]));
                } else {
                    m.appendReplacement(buffer, String.valueOf(args[argIndex]));
                }
                argIndex++;
            } else {
                m.appendReplacement(buffer, NULL_STR);
            }
        }
        return buffer.toString();
    }

}
