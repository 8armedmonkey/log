package log.benchmark.formatter;

import bb.util.Benchmark;
import bb.util.MemoryMeasurer;
import log.formatter.MessageFormatter;
import log.util.Hex;

import static java.lang.String.format;
import static java.lang.System.out;

public class FormatterBenchmark {

    private static final String FORMAT = "Text text, text = {}, text = {}, {}, {}; Text: {}.";
    private static final Object[] ARGS = {
            Hex.toHexString(new byte[]{
                    (byte) 0xAA, (byte) 0xBB, (byte) 0xCC, (byte) 0xDD,
                    (byte) 0xEE, (byte) 0xFF, (byte) 0x11, (byte) 0x22,
                    (byte) 0x33, (byte) 0x44, (byte) 0x55, (byte) 0x66,
                    (byte) 0x77, (byte) 0x88, (byte) 0x99, (byte) 0x00,
            }),
            1234567890L, 1, 2.0, "string"
    };

    private static final int ATTEMPTS = 100000;

    private static final MessageFormatter[] FORMATTER_LIST = new MessageFormatter[]{
            new MessageFormatterImpl1(),
            new MessageFormatterImpl2(),
            new MessageFormatterImpl3(),
            new MessageFormatterImplLog4J()
    };

    public static void main(String[] args) {
        simpleBenchmark();
        benchmark();
    }

    private static void simpleWarmUpJvm() {
        long n = 1;

        for (long start = System.nanoTime(); System.nanoTime() - start < 10.0 * 1e9; n *= 2) {
            for (MessageFormatter formatter : FORMATTER_LIST) {
                for (int i = 0; i < n; i++) {
                    formatter.format(FORMAT, ARGS);
                }
            }
        }
    }

    private static void simpleBenchmark() {
        MemoryMeasurer.restoreJvm();

        out.println("Simple warm up JVM");
        out.println();
        simpleWarmUpJvm();

        long total, start, end;
        float average;

        for (MessageFormatter formatter : FORMATTER_LIST) {
            out.println(format("Simple benchmarking formatter %s:", formatter.getClass().getSimpleName()));

            total = 0;

            for (int i = 0; i < ATTEMPTS; i++) {
                start = System.currentTimeMillis();
                formatter.format(FORMAT, ARGS);
                end = System.currentTimeMillis();
                total += end - start;
            }

            average = (float) total / ATTEMPTS;

            out.println(format("Result: %s", formatter.format(FORMAT, ARGS)));
            out.println(format("Total time: %d", total));
            out.println(format("Average time: %f", average));
            out.println("-----");
        }
    }

    private static void benchmark() {
        for (MessageFormatter formatter : FORMATTER_LIST) {
            out.println(format("Benchmarking formatter %s:", formatter.getClass().getSimpleName()));

            try {
                out.println(new Benchmark(() -> formatter.format(FORMAT, ARGS)));
                out.println("-----");
            } catch (Exception e) {
                // Ignore error.
            }
        }
    }

}
