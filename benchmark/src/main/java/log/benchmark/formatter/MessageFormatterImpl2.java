package log.benchmark.formatter;

import log.formatter.MessageFormatter;
import log.util.Hex;

import java.io.IOException;

public class MessageFormatterImpl2 implements MessageFormatter {

    private static final String NULL_STR = "null";

    @Override
    public String format(String format, Object... args) {
        if (args != null && args.length > 0) {
            try {
                return formatInternal(format, args);
            } catch (IOException e) {
                return format;
            }
        }
        return format;
    }

    private String formatInternal(String format, Object... args) throws IOException {
        StringBuilder builder = new StringBuilder();
        char[] chars = format.toCharArray();
        boolean placeholderOpen = false;
        int argIndex = 0;

        for (char c : chars) {
            switch (c) {
                case '{':
                    builder.append(c);
                    placeholderOpen = true;
                    break;
                case '}':
                    if (placeholderOpen) {
                        replacePlaceholder(builder, argIndex, args);
                        argIndex++;
                    }
                    placeholderOpen = false;
                    break;
                default:
                    builder.append(c);
                    placeholderOpen = false;
                    break;
            }
        }
        return builder.toString();
    }

    private void replacePlaceholder(StringBuilder builder, int argIndex, Object... args) {
        builder.deleteCharAt(builder.length() - 1);

        if (argIndex < args.length) {
            Object arg = args[argIndex];

            if (args[argIndex] instanceof byte[]) {
                builder.append(Hex.toHexString((byte[]) arg));
            } else {
                builder.append(String.valueOf(arg));
            }
        } else {
            builder.append(NULL_STR);
        }
    }

}
