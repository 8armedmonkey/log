package log.benchmark.trace;

import java.util.ArrayList;
import java.util.List;

public class TraceImpl1 {

    private static String getMethodName(Throwable throwable) {
        StackTraceElement stackTraceElement = throwable.getStackTrace()[1];
        return stackTraceElement.getMethodName();
    }

    private List<String> entries;

    public TraceImpl1() {
        entries = new ArrayList<>();
    }

    public void enter() {
        entries.add(String.format("%s -- enter", getMethodName(new Throwable())));
    }

    public void exit() {
        entries.add(String.format("%s -- exit", getMethodName(new Throwable())));
    }

    public List<String> getEntries() {
        return entries;
    }

    public void clearEntries() {
        entries.clear();
    }

}
