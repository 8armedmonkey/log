package log.benchmark.trace;

import bb.util.MemoryMeasurer;

import static java.lang.System.out;

public class TraceBenchmark {

    private static final int ATTEMPTS = 100000;

    public static void main(String[] args) {
        simpleBenchmark();
    }

    private static void simpleWarmUpJvm() {
        long n = 1;

        for (long start = System.nanoTime(); System.nanoTime() - start < 10.0 * 1e9; n *= 2) {
            benchmarkImpl1(n);
            benchmarkImpl2(n);
        }
    }

    private static void simpleBenchmark() {
        MemoryMeasurer.restoreJvm();

        out.println("Simple warm up JVM");
        out.println();
        simpleWarmUpJvm();

        out.println("Start benchmark 1");
        measure(() -> benchmarkImpl1(ATTEMPTS));

        out.println("Start benchmark 2");
        measure(() -> benchmarkImpl2(ATTEMPTS));
    }

    private static void measure(Runnable runnable) {
        long st = System.currentTimeMillis();

        runnable.run();

        long et = System.currentTimeMillis();

        out.println(String.format("Total duration: %d ms.", (et - st)));
        out.println(String.format("Average duration: %.4f ms.", (float) (et - st) / ATTEMPTS));
        out.println();
    }

    private static void benchmarkImpl1(long n) {
        TraceTarget1 t = new TraceTarget1();

        for (int i = 0; i < n; i++) {
            t.foo();
        }
    }

    private static void benchmarkImpl2(long n) {
        TraceTarget2 t = new TraceTarget2();

        for (int i = 0; i < n; i++) {
            t.bar();
        }
    }

}
