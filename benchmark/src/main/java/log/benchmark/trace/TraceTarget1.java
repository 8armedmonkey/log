package log.benchmark.trace;

public class TraceTarget1 {

    private TraceImpl1 traceImpl1;

    public TraceTarget1() {
        traceImpl1 = new TraceImpl1();
    }

    public void foo() {
        traceImpl1.enter();
        traceImpl1.exit();
    }

    public void printEntries() {
        System.out.println("TraceTarget1.printEntries...");

        for (String entry : traceImpl1.getEntries()) {
            System.out.println(entry);
        }
    }

}
