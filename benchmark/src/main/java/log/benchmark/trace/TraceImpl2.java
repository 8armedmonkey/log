package log.benchmark.trace;

import java.util.ArrayList;
import java.util.List;

public class TraceImpl2 {

    private List<String> entries;

    public TraceImpl2() {
        entries = new ArrayList<>();
    }

    private static String getMethodName(Thread thread) {
        return thread.getStackTrace()[3].getMethodName();
    }

    public void enter() {
        entries.add(String.format("%s -- enter", getMethodName(Thread.currentThread())));
    }

    public void exit() {
        entries.add(String.format("%s -- exit", getMethodName(Thread.currentThread())));
    }

    public List<String> getEntries() {
        return entries;
    }

    public void clearEntries() {
        entries.clear();
    }

}
