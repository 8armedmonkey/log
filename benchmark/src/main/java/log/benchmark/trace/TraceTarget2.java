package log.benchmark.trace;

public class TraceTarget2 {

    private TraceImpl2 traceImpl2;

    public TraceTarget2() {
        traceImpl2 = new TraceImpl2();
    }

    public void bar() {
        traceImpl2.enter();
        traceImpl2.exit();
    }

    public void printEntries() {
        System.out.println("TraceTarget2.printEntries...");

        for (String entry : traceImpl2.getEntries()) {
            System.out.println(entry);
        }
    }

}
