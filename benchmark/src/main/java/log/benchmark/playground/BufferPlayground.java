package log.benchmark.playground;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class BufferPlayground {

    public static void main(String[] args) {
        byte[] b1 = {0x01, 0x02, 0x03, 0x04};
        byte[] b2 = {0x05, 0x06, 0x07, 0x08};

        ByteBuffer bb = ByteBuffer.allocate(1024);
        printBufferStats("Initial state", bb);

        bb.put(b1);
        printBufferStats("Putting 4 bytes into the buffer.", bb);

        bb.put(b2);
        printBufferStats("Putting another 4 bytes into the buffer.", bb);

        bb.flip();
        printBufferStats("Flipping the buffer.", bb);

        bb.clear();
        printBufferStats("Clearing the buffer.", bb);

        bb.put(b1);
        printBufferStats("Putting 4 bytes into the buffer.", bb);
    }

    private static void printBufferStats(String message, Buffer buffer) {
        System.out.println(message);
        System.out.println("------------------------------");
        System.out.println("hasRemaining: " + buffer.hasRemaining());
        System.out.println("remaining: " + buffer.remaining());
        System.out.println("position: " + buffer.position());
        System.out.println("limit: " + buffer.limit());
        System.out.println("capacity: " + buffer.capacity());
        System.out.println();
    }

}
