package log.util;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    /**
     * Create the file if it does not exist yet.
     * It will also try to create the parent directory if it does not exist.
     *
     * @param fileName The name of the file to be created.
     * @return True if the file exists or successfully created, false otherwise.
     * @throws IOException IO operation error.
     */
    public static boolean createFileIfNotExists(String fileName) throws IOException {
        File file = new File(fileName);

        if (!file.exists()) {
            File parent = file.getParentFile();

            if (parent != null && !parent.exists()) {
                return parent.mkdirs() && file.createNewFile();
            } else {
                return file.createNewFile();
            }
        } else {
            return true;
        }
    }

    /**
     * Delete the specified file.
     *
     * @param fileName The name of the file to be deleted.
     * @return True if successful, false otherwise.
     */
    public static boolean deleteFile(String fileName) {
        return new File(fileName).delete();
    }

    /**
     * Delete the specified file and recreate it.
     *
     * @param fileName The name of the file to be recreated.
     * @return True if successful, false otherwise.
     * @throws IOException IO operation error.
     */
    public static boolean recreateFile(String fileName) throws IOException {
        File file = new File(fileName);

        if (file.exists()) {
            return deleteFile(fileName) && createFileIfNotExists(fileName);
        } else {
            return createFileIfNotExists(fileName);
        }
    }

}
