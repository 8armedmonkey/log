package log.util;

public class IntUtils {

    private static final int[] sizeTable = {9, 99, 999, 9999, 99999, 999999, 9999999,
            99999999, 999999999, Integer.MAX_VALUE};

    /**
     * Shamelessly copied from Java Integer.
     *
     * @param x Positive integer.
     * @return The string size of the integer.
     */
    public static int stringSize(int x) {
        for (int i = 0; ; i++)
            if (x <= sizeTable[i])
                return i + 1;
    }

}
