package log.util;

public class StringUtils {

    /**
     * Append a string to the StringBuilder with left-padding if needed.
     *
     * @param sb     StringBuilder where the string will be appended to.
     * @param string String to be appended to the StringBuilder.
     * @param width  The desired field width.
     * @param c      The character used as padding.
     */
    public static void appendWithPadLeft(StringBuilder sb, String string, int width, char c) {
        if (string.length() < width) {
            for (int i = 0, n = width - string.length(); i < n; i++) {
                sb.append(c);
            }
            sb.append(string);
        } else {
            sb.append(string);
        }
    }

    /**
     * Append a string to the StringBuilder with right-padding if needed.
     *
     * @param sb     StringBuilder where the string will be appended to.
     * @param string String to be appended to the StringBuilder.
     * @param width  The desired field width.
     * @param c      The character used as padding.
     */
    public static void appendWithPadRight(StringBuilder sb, String string, int width, char c) {
        if (string.length() < width) {
            sb.append(string);

            for (int i = 0, n = width - string.length(); i < n; i++) {
                sb.append(c);
            }
        } else {
            sb.append(string);
        }
    }

    /**
     * Append a string to the StringBuilder.
     * If the string is longer than the specified width, it will be truncated.
     * If the string is shorter than the specified width, it will be left-padded.
     *
     * @param sb     StringBuilder where the string will be appended to.
     * @param string String to be appended to the StringBuilder.
     * @param width  The desired field width.
     * @param c      The character used as padding.
     */
    public static void appendWithTruncateOrPadLeft(StringBuilder sb, String string, int width, char c) {
        if (string.length() < width) {
            appendWithPadLeft(sb, string, width, c);
        } else {
            appendTruncateWidth(sb, string, width);
        }
    }

    /**
     * Append a string to the StringBuilder.
     * If the string is longer than the specified width, it will be truncated.
     * If the string is shorter than the specified width, it will be right-padded.
     *
     * @param sb     StringBuilder where the string will be appended to.
     * @param string String to be appended to the StringBuilder.
     * @param width  The desired field width.
     * @param c      The character used as padding.
     */
    public static void appendWithTruncateOrPadRight(StringBuilder sb, String string, int width, char c) {
        if (string.length() < width) {
            appendWithPadRight(sb, string, width, c);
        } else {
            appendTruncateWidth(sb, string, width);
        }
    }

    /**
     * Append integer to StringBuilder. If the integer's string width is less than width,
     * it will be padded with the specified character in front.
     *
     * @param sb    StringBuilder where the integer will be appended to.
     * @param digit Integer to be appended to the StringBuilder.
     * @param width The desired field width.
     * @param c     The character used as padding.
     */
    public static void appendDigitWithPad(StringBuilder sb, int digit, int width, char c) {
        int stringSize = IntUtils.stringSize(digit);

        if (stringSize < width) {
            for (int i = 0, n = width - stringSize; i < n; i++) {
                sb.append(c);
            }
            sb.append(digit);
        } else {
            sb.append(digit);
        }
    }

    private static void appendTruncateWidth(StringBuilder sb, String string, int width) {
        for (int i = 0, n = Math.min(string.length(), width); i < n; i++) {
            sb.append(string.charAt(i));
        }
    }

}
