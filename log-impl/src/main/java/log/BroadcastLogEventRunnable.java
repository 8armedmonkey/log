package log;

class BroadcastLogEventRunnable implements Runnable {

    private final Logger logger;
    private final LogEvent logEvent;

    BroadcastLogEventRunnable(Logger logger, LogEvent logEvent) {
        this.logger = logger;
        this.logEvent = logEvent;
    }

    @Override
    public void run() {
        ((LoggerImpl) logger).broadcastLogEvent(logEvent);
    }

}
