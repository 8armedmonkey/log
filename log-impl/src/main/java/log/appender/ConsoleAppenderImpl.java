package log.appender;

import log.LogEvent;
import log.layout.Layout;

import java.io.PrintStream;

public class ConsoleAppenderImpl extends BaseAppender implements ConsoleAppender {

    private PrintStream printStream;

    public ConsoleAppenderImpl(String name, Layout layout) {
        this(name, layout, System.out);
    }

    public ConsoleAppenderImpl(String name, Layout layout, PrintStream printStream) {
        this.name = name;
        this.layout = layout;
        this.printStream = printStream;
    }

    @Override
    public void append(LogEvent event) {
        printStream.print(layout.process(event));
    }

}
