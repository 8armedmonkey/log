package log.appender;

import log.layout.Layout;

public abstract class BaseAppender implements Appender {

    protected Layout layout;
    protected String name;

    @Override
    public Layout getLayout() {
        return layout;
    }

    @Override
    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

}
