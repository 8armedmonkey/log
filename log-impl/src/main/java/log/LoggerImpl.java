package log;

import log.appender.Appender;
import log.appender.FileAppender;
import log.formatter.MessageFormatter;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.currentTimeMillis;

public class LoggerImpl implements Logger {

    private Map<String, Appender> appenderRegister;
    private LogLevel logLevel;
    private MessageFormatter messageFormatter;
    private ExecutorService executorService;

    public LoggerImpl(LogLevel logLevel, MessageFormatter messageFormatter) {
        this.appenderRegister = new HashMap<>();
        this.logLevel = logLevel;
        this.messageFormatter = messageFormatter;
        this.executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    public void e(String tag, String format, @Nullable Object... args) {
        logIfEnabled(LogLevel.ERROR, tag, format, args);
    }

    @Override
    public void e(String tag, Throwable throwable) {
        logIfEnabled(LogLevel.ERROR, tag, throwable);
    }

    @Override
    public void i(String tag, String format, @Nullable Object... args) {
        logIfEnabled(LogLevel.INFO, tag, format, args);
    }

    @Override
    public void d(String tag, String format, @Nullable Object... args) {
        logIfEnabled(LogLevel.DEBUG, tag, format, args);
    }

    @Override
    public void enter(String tag, String format, @Nullable Object... args) {
        logIfEnabled(LogLevel.TRACE, tag, format, args);
    }

    @Override
    public void exit(String tag, String format, @Nullable Object... args) {
        logIfEnabled(LogLevel.TRACE, tag, format, args);
    }

    @Override
    public synchronized void start() {
        if (executorService.isShutdown()) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    @Override
    public synchronized void shutdown() {
        flush();
        executorService.shutdownNow();
    }

    @Override
    public void addAppender(Appender appender) {
        appenderRegister.put(appender.getName(), appender);
    }

    @Override
    public void removeAppender(Appender appender) {
        removeAppender(appender.getName());
    }

    @Override
    public void removeAppender(String name) {
        appenderRegister.remove(name);
    }

    @Override
    public boolean containAppender(Appender appender) {
        return containAppender(appender.getName());
    }

    @Override
    public boolean containAppender(String name) {
        return appenderRegister.containsKey(name);
    }

    void broadcastLogEvent(LogEvent event) {
        Collection<Appender> appenderCollection = appenderRegister.values();

        for (Appender appender : appenderCollection) {
            appender.append(event);
        }
    }

    private void logIfEnabled(LogLevel logLevel, String tag, String format, @Nullable Object... args) {
        if (this.logLevel.isMorePermissiveThan(logLevel)) {
            LogEvent event = new LogEvent.Builder()
                    .timeMillis(currentTimeMillis())
                    .logLevel(logLevel)
                    .tag(tag)
                    .message(messageFormatter.format(format, args))
                    .build();

            executorService.submit(new BroadcastLogEventRunnable(this, event));
        }
    }

    private void logIfEnabled(LogLevel logLevel, String tag, Throwable throwable) {
        if (this.logLevel.isMorePermissiveThan(logLevel)) {
            LogEvent event = new LogEvent(currentTimeMillis(), logLevel, tag, throwable);
            broadcastLogEvent(event);
        }
    }

    private void flush() {
        Collection<Appender> appenderCollection = appenderRegister.values();

        for (Appender appender : appenderCollection) {
            if (appender instanceof FileAppender) {
                ((FileAppender) appender).flush();
            }
        }
    }

}
