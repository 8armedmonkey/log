package log.layout;

import log.LogEvent;
import log.LogLevel;
import log.util.ColorUtils;

public class ColorLineLayout extends LineLayout {

    private static String getAnsiColor(LogLevel level) {
        switch (level) {
            case ERROR:
                return ColorUtils.ANSI_RED;
            case INFO:
                return ColorUtils.ANSI_YELLOW;
            case DEBUG:
                return ColorUtils.ANSI_BLUE;
            case TRACE:
                return ColorUtils.ANSI_PURPLE;
        }
        return ColorUtils.ANSI_RESET;
    }

    @Override
    protected void appendDateTime(LogEvent event) {
        stringBuilder.append(getAnsiColor(event.getLogLevel()));
        super.appendDateTime(event);
    }

    @Override
    protected void appendMessage(LogEvent event) {
        super.appendMessage(event);
        stringBuilder.append(ColorUtils.ANSI_RESET);
    }

    @Override
    protected void appendThrowable(LogEvent event) {
        stringBuilder.append(getAnsiColor(event.getLogLevel()));
        super.appendThrowable(event);
        stringBuilder.append(ColorUtils.ANSI_RESET);
    }

}
