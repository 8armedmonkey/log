package log.layout;

import log.LogEvent;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.TimeZone;

import static log.util.StringUtils.*;

/**
 * Layout the log event as: [Timestamp] [Log Level] [Tag] [Message].
 * <p>
 * If the log event has a Throwable, it will be printed directly below the line.
 */
public class LineLayout implements Layout {

    private static final int COL_WIDTH_LOG_LEVEL = 5;
    private static final int COL_WIDTH_TAG = 15;

    private static final String COL_SEPARATOR = " | ";
    private static final String LINE_SEPARATOR = System.lineSeparator();
    private static final String DATE_SEPARATOR = "-";
    private static final String TIME_SEPARATOR = ":";
    private static final String MILLIS_SEPARATOR = ",";
    private static final char SPACE = ' ';
    private static final char PADDING_CHARACTER = SPACE;
    private static final char PADDING_DIGIT = '0';

    protected StringBuilder stringBuilder;
    private Calendar calendar;
    private StringWriter stringWriter;
    private PrintWriter printWriter;

    public LineLayout() {
        calendar = Calendar.getInstance(TimeZone.getDefault());
        stringBuilder = new StringBuilder();
        stringWriter = new StringWriter();
        printWriter = new PrintWriter(stringWriter);
    }

    @Override
    public synchronized String process(LogEvent event) {
        prepareProcessing();
        appendDateTime(event);
        appendLogLevel(event);
        appendTag(event);
        appendMessage(event);

        if (event.getThrowable() != null) {
            appendThrowable(event);
        }

        return stringBuilder.toString();
    }

    protected void prepareProcessing() {
        stringBuilder.setLength(0);
    }

    protected void appendDateTime(LogEvent event) {
        calendar.setTimeInMillis(event.getTimeMillis());

        int y = calendar.get(Calendar.YEAR);
        int M = calendar.get(Calendar.MONTH) + 1;
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        int H = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int s = calendar.get(Calendar.SECOND);
        int S = calendar.get(Calendar.MILLISECOND);

        stringBuilder.append(y);
        stringBuilder.append(DATE_SEPARATOR);

        appendDigitWithPad(stringBuilder, M, 2, PADDING_DIGIT);
        stringBuilder.append(DATE_SEPARATOR);

        appendDigitWithPad(stringBuilder, d, 2, PADDING_DIGIT);
        stringBuilder.append(SPACE);

        appendDigitWithPad(stringBuilder, H, 2, PADDING_DIGIT);
        stringBuilder.append(TIME_SEPARATOR);

        appendDigitWithPad(stringBuilder, m, 2, PADDING_DIGIT);
        stringBuilder.append(TIME_SEPARATOR);

        appendDigitWithPad(stringBuilder, s, 2, PADDING_DIGIT);
        stringBuilder.append(MILLIS_SEPARATOR);

        appendDigitWithPad(stringBuilder, S, 3, PADDING_DIGIT);
        stringBuilder.append(COL_SEPARATOR);
    }

    protected void appendLogLevel(LogEvent event) {
        appendWithPadRight(stringBuilder, event.getLogLevel().name(), COL_WIDTH_LOG_LEVEL, PADDING_CHARACTER);
        stringBuilder.append(COL_SEPARATOR);
    }

    protected void appendTag(LogEvent event) {
        appendWithTruncateOrPadRight(stringBuilder, event.getTag(), COL_WIDTH_TAG, PADDING_CHARACTER);
        stringBuilder.append(COL_SEPARATOR);
    }

    protected void appendMessage(LogEvent event) {
        stringBuilder.append(event.getMessage());
        stringBuilder.append(LINE_SEPARATOR);
    }

    protected void appendThrowable(LogEvent event) {
        stringWriter.getBuffer().setLength(0);
        event.getThrowable().printStackTrace(printWriter);

        stringBuilder.append(stringWriter.toString());
        stringBuilder.append(LINE_SEPARATOR);
    }

}
