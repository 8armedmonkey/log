package log.formatter;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestFormatter {

    private static MessageFormatter formatter;

    @BeforeClass
    public static void setUpBeforeClass() {
        formatter = new MessageFormatterImpl();
    }

    @Test
    public void format_sameAmountOfPlaceholdersAndArgs_allPlaceholdersReplaced() {
        int arg1 = 1;
        float arg2 = 2.0f;
        String arg3 = "string";
        byte[] arg4 = new byte[]{(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF};

        String format = "Text ({}). Text={}, text={}, {}.";
        String expected = "Text (1). Text=2.0, text=string, 123456ABCDEF.";
        String actual = formatter.format(format, arg1, arg2, arg3, arg4);
        assertEquals(expected, actual);
    }

    @Test
    public void format_morePlaceholdersThanArgs_placeholdersWithMissingArgsAreReplacedWithNull() {
        int arg1 = 1;
        float arg2 = 2.0f;

        String format = "Text ({}). Text={}, text={}, {}.";
        String expected = "Text (1). Text=2.0, text=null, null.";
        String actual = formatter.format(format, arg1, arg2);
        assertEquals(expected, actual);
    }

    @Test
    public void format_lessPlaceholdersThanArgs_remainingArgsAreIgnored() {
        String format = "Text ({}).";
        String expected = "Text (null).";
        String actual = formatter.format(format);
        assertEquals(expected, actual);
    }

    @Test
    public void format_noPlaceholders_stringIsReturnedAsIs() {
        String format = "Text text text.";
        String actual = formatter.format(format);
        assertEquals(format, actual);
    }

    @Test
    public void format_noPlaceholdersWithArgs_stringIsReturnedAsIs() {
        int arg1 = 1;
        float arg2 = 2.0f;

        String format = "Text text text.";
        String actual = formatter.format(format, arg1, arg2);
        assertEquals(format, actual);
    }

}
