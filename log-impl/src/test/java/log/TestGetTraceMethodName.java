package log;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestGetTraceMethodName {

    @Test
    public void trace_correctOrderIsReturned() {
        Trace trace = new Trace();
        TestClass testClass = new TestClass(trace);

        testClass.foo();
        testClass.bar();
        testClass.bar();
        testClass.foo();

        assertEquals(8, trace.getEntries().size());
        assertEquals("foo enter", trace.getEntries().get(0));
        assertEquals("foo exit", trace.getEntries().get(1));
        assertEquals("bar enter", trace.getEntries().get(2));
        assertEquals("bar exit", trace.getEntries().get(3));
        assertEquals("bar enter", trace.getEntries().get(4));
        assertEquals("bar exit", trace.getEntries().get(5));
        assertEquals("foo enter", trace.getEntries().get(6));
        assertEquals("foo exit", trace.getEntries().get(7));
    }


    private static class Trace {

        private static String getTraceMethodName(Throwable throwable) {
            StackTraceElement stackTraceElement = throwable.getStackTrace()[1];
            return stackTraceElement.getMethodName();
        }

        private List<String> entries;

        Trace() {
            entries = new ArrayList<>();
        }

        void enter() {
            entries.add(String.format("%s %s", getTraceMethodName(new Throwable()), "enter"));
        }

        void exit() {
            entries.add(String.format("%s %s", getTraceMethodName(new Throwable()), "exit"));
        }

        List<String> getEntries() {
            return entries;
        }

    }

    private static class TestClass {

        private Trace trace;

        TestClass(Trace trace) {
            this.trace = trace;
        }

        void foo() {
            trace.enter();
            trace.exit();
        }

        void bar() {
            trace.enter();
            trace.exit();
        }

    }

}
