package log.appender;

import log.LogEvent;
import log.layout.LineLayout;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static java.lang.System.currentTimeMillis;
import static log.LogLevel.ERROR;
import static org.junit.Assert.assertTrue;

public class TestConsoleAppender {

    @Test
    public void append_logEvent_messageIsAppended() throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(os);
        ConsoleAppender ca = new ConsoleAppenderImpl("Console", new LineLayout(), printStream);

        String tag = "Tag";
        String text = "Text";

        ca.append(new LogEvent.Builder()
                .timeMillis(currentTimeMillis())
                .logLevel(ERROR)
                .tag(tag)
                .message(text)
                .build());

        String line = new String(os.toByteArray(), "UTF-8");

        assertTrue(line.contains(tag));
        assertTrue(line.contains(text));
    }

}
