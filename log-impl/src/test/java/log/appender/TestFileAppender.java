package log.appender;

import log.LogEvent;
import log.layout.LineLayout;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.System.currentTimeMillis;
import static log.LogLevel.ERROR;
import static log.util.FileUtils.deleteFile;
import static log.util.FileUtils.recreateFile;
import static org.junit.Assert.assertTrue;

public class TestFileAppender {

    @Test
    public void append_logEvent_messageIsAppended() throws IOException {
        String fileName = "src/test/resources/i/appender/append_logEvent_messageIsAppended.txt";
        recreateFile(fileName);

        String tag = "Tag";
        String text = "Text";

        FileAppender fa = new FileAppenderImpl("File", new LineLayout(), fileName, 1024);

        fa.append(new LogEvent.Builder()
                .timeMillis(currentTimeMillis())
                .logLevel(ERROR)
                .tag(tag)
                .message(text)
                .build());

        fa.flush();
        fa.close();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = br.readLine();
        br.close();

        assertTrue(line.contains(tag));
        assertTrue(line.contains(text));
        deleteFile(fileName);
    }

}
