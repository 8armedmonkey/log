package log.layout;

import log.LogEvent;
import org.junit.Test;

import java.util.TimeZone;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static log.LogLevel.ERROR;
import static org.junit.Assert.assertEquals;

public class TestLineLayout {

    static {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Amsterdam"));
    }

    @Test
    public void process_logEvent_laidOutProperly() {
        String expected = format(
                "2017-01-12 16:11:55,470 | ERROR | TAG             | An error has occurred.%s", lineSeparator());

        String actual = new LineLayout().process(new LogEvent.Builder()
                .timeMillis(1484233915470L)
                .logLevel(ERROR)
                .tag("TAG")
                .message("An error has occurred.")
                .build());

        assertEquals(expected, actual);
    }

}
