package log;

import log.appender.Appender;
import log.formatter.MessageFormatter;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class TestLogger {

    @Test
    public void addAppender_appender_appenderIsAdded() {
        Logger logger = new LoggerImpl(LogLevel.ALL, mock(MessageFormatter.class));
        Appender appender = mockAppenderWithName("Console");

        logger.addAppender(appender);

        assertTrue(logger.containAppender(appender));
    }

    @Test
    public void removeAppender_appender_appenderIsRemoved() {
        Logger logger = new LoggerImpl(LogLevel.ALL, mock(MessageFormatter.class));
        Appender appender = mockAppenderWithName("Console");

        logger.addAppender(appender);
        logger.removeAppender(appender);

        assertFalse(logger.containAppender(appender));
    }

    @Test
    public void info_message_allAppendersAppendLogEvent() {
        Logger logger = new LoggerImpl(LogLevel.ALL, mock(MessageFormatter.class));
        logger.setLogLevel(LogLevel.ALL);

        Appender appender1 = mockAppenderWithName("Console");
        Appender appender2 = mockAppenderWithName("File");

        logger.addAppender(appender1);
        logger.addAppender(appender2);

        logger.i("InfoTag1", "Info 1.");
        logger.i("InfoTag2", "Info 2.");
        logger.e("ErrorTag", "Error.");

        verify(appender1, times(3)).append(any(LogEvent.class));
        verify(appender2, times(3)).append(any(LogEvent.class));
    }

    @Test
    public void info_message_lowerLevelMessagesAreAppended() {
        Logger logger = new LoggerImpl(LogLevel.ALL, mock(MessageFormatter.class));
        logger.setLogLevel(LogLevel.INFO);

        Appender appender = mockAppenderWithName("File");
        logger.addAppender(appender);

        logger.e("ErrorTag", "Error message.");
        logger.i("InfoTag", "Info message.");

        verify(appender, times(2)).append(any(LogEvent.class));
    }

    @Test
    public void info_message_higherLevelMessagesAreNotAppended() {
        Logger logger = new LoggerImpl(LogLevel.ALL, mock(MessageFormatter.class));
        logger.setLogLevel(LogLevel.INFO);

        Appender appender = mockAppenderWithName("File");
        logger.addAppender(appender);

        logger.d("DebugTag", "Debug message.");
        logger.enter("TraceTag", "Enter message.");
        logger.exit("TraceTag", "Exit message.");

        verify(appender, never()).append(any(LogEvent.class));
    }

    private static Appender mockAppenderWithName(String name) {
        Appender appender = mock(Appender.class);
        when(appender.getName()).thenReturn(name);
        return appender;
    }

}
