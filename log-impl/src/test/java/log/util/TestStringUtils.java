package log.util;

import org.junit.Test;

import static log.util.StringUtils.appendWithPadLeft;
import static log.util.StringUtils.appendWithPadRight;
import static org.junit.Assert.assertEquals;

public class TestStringUtils {

    @Test
    public void appendWithPadLeft_originalLengthShorterThanDesiredLength_paddedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "12345";
        appendWithPadLeft(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "*****12345";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithPadLeft_originalLengthExceedsDesiredLength_appended() {
        StringBuilder sb = new StringBuilder();
        String s = "1234567890";
        appendWithPadLeft(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "1234567890";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithPadRight_originalLengthShorterThanDesiredLength_paddedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "12345";
        appendWithPadRight(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "12345*****";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithPadRight_originalLengthExceedsDesiredLength_appended() {
        StringBuilder sb = new StringBuilder();
        String s = "1234567890";
        appendWithPadRight(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "1234567890";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithTruncateOrPadLeft_originalLengthShorterThanDesiredLength_paddedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "12345";
        StringUtils.appendWithTruncateOrPadLeft(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "*****12345";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithTruncateAndPadLeft_originalLengthExceedsDesiredLength_truncatedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "123456789012345";
        StringUtils.appendWithTruncateOrPadLeft(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "1234567890";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithTruncateOrPadRight_originalLengthShorterThanDesiredLength_paddedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "12345";
        StringUtils.appendWithTruncateOrPadRight(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "12345*****";

        assertEquals(expected, actual);
    }

    @Test
    public void appendWithTruncateAndPadRight_originalLengthExceedsDesiredLength_truncatedAndAppended() {
        StringBuilder sb = new StringBuilder();
        String s = "123456789012345";
        StringUtils.appendWithTruncateOrPadRight(sb, s, 10, '*');

        String actual = sb.toString();
        String expected = "1234567890";

        assertEquals(expected, actual);
    }

}
